const canvas = document.querySelector("#draw");
const ctx = canvas.getContext("2d");

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

ctx.strokeStyle = "#004473";
ctx.lineJion = "round"
ctx.lineCap = "round"
ctx.lineWidth = 100;
ctx.globalCompositeOperation = "multiply";

let isDrawing = false; 
let lastX = 0;
let lastY = 0;
let hue = 0;
let direction = true;

function draw(ev) {
    if(!isDrawing) return;
    console.log(ev);
    ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
    ctx.beginPath();
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(ev.offsetX, ev.offsetY);
    ctx.stroke();
    lastX = ev.offsetX;
    lastY = ev.offsetY;

    hue++;
        if(hue >= 360) {
            hue = 0;
        }

    if(ctx.lineWidth >= 100 || ctx.lineWidth <= 1) {
        direction = !direction;
    }
    if(direction) {
        ctx.lineWidth++;
    } else {
        ctx.lineWidth--;
    }

}

canvas.addEventListener("mousemove", draw);
canvas.addEventListener("mousedown", (ev) => {
    isDrawing = true;
        lastX = ev.offsetX;
        lastY = ev.offsetY;
});
canvas.addEventListener("mouseup", () => isDrawing = false);
canvas.addEventListener("mouseout", () => isDrawing = false);